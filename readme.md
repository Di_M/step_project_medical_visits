**Step Project Advanced JavaScript**

# **Medical Visits**

**Creators:** _Elena Voitova and Dmytro Murashko_ <br>
**Tested by:** _Elena Voitova and Dmytro Murashko_ <br>

Login credentials <br>
email: VoMu_team@email.com <br>
pass: Welcome135! <br>

Tasks of this project:
1. <strong>HTML/CSS page.</strong><br>
1.1. Create **Header and Main Section for Visit cards** (html and css). <br> Creators: Dmytro Murashko and Elena Voitova<br>
1.3. Create **Modal window visit cards**. <br> Creators: Elena Voitova <br>

2. <strong>JavaScript logic.</strong><br>
2.1. Create **class "Visit"** <br> Creator:s Elena Voitova <br>
2.2. Create **class "Сardiologist"** <br> Creators: Elena Voitova. Assistants: Dmytro Murashko <br>
2.3. Create **class "Dentist"** <br> Creators: Elena Voitova. Assistants: Dmytro Murashko <br>
2.4. Create **class "Therapist"** <br> Creators: Elena Voitova. Assistants: Dmytro Murashko <br>
2.5. Create **class "Modal, EditModal and CreateModal"** <br> Creators: Elena Voitova. <br>
2.6. Create **Main.js** <br> Creators: Elena Voitova and Dmytro Murashko <br>

3. <strong>Drag&Drop</strong><br>
3.1. **Drag&Drop** events for all cards which on the table. <br> Creators: Dmytro Murashko <br>

4. <strong>AJAX-request and LocalStorage</strong><br>
4.1. **GET**-request.<br> Creators: Dmytro Murashko <br>
4.2. **POST**-request.<br> Creators: Dmytro Murashko <br>
4.3. **DELETE**-request.<br> Creators: Dmytro Murashko <br>
4.4. **PUT**-request.<br> Creators: Dmytro Murashko <br>
4.5. **LocalStorage** with AJAX-request. <br> Creators: Dmytro Murashko <br>

Run project: <br>
Just run **"index.html"** in your browser.)